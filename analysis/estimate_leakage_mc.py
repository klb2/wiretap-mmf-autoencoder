import os
import argparse

import numpy as np
from joblib import Parallel, cpu_count, delayed
from digcommpy import information_theory as it
from digcommpy import parsers

def estimate_differential_entropy(codebook, noise_var, samples, mdl_db=20, sort_mdl=True, normalize=False):
    codewords = np.array(list(codebook.values()))
    n = np.shape(codewords)[1]
    all_samples = np.empty((0, n))
    _entropy_list = []
    mdl = 10**(mdl_db/10.)
    vmax = 1
    vmin = vmax/mdl
    for _ in range(1000):
        v = (vmax-vmin)*np.random.rand(n) + vmin
        if sort_mdl:
            v = np.sort(v)[::-1]
        if normalize:
            v = n*v/K.sum(v)
        trans_codewords = v * codewords
        gauss_mix = it.GaussianMixtureRv(trans_codewords, noise_var)
        _samples = gauss_mix.rvs(samples)
        #all_samples = np.append(all_samples, _samples, axis=0)
        entropy = gauss_mix.logpdf(_samples)
        entropy = -np.mean(entropy)
        _entropy_list.append(entropy)
    #gauss_mix = it.GaussianMixtureRv(list(codebook.values()), noise_var)
    #_samples = gauss_mix.rvs(samples)
    #print(np.shape(all_samples))
    #entropy = gauss_mix.logpdf(all_samples)
    #entropy = -np.mean(entropy)
    entropy = np.mean(_entropy_list)
    return entropy

def estimate_leakage_from_codebook(codebook, noise_var, samples, channel_options=None):
    if channel_options is None:
        channel_options = {'sort_mdl': True,
                           'normalize': False}
    codebook, code_info = codebook
    #h_z = estimate_differential_entropy(codebook, noise_var, samples, **channel_options)
    h_z = estimate_differential_entropy(codebook, noise_var, 2*samples, **channel_options)
    h_zm = []
    info_length = code_info['info_length']
    random_length = code_info['random_length']
    for message in range(2**info_length):
        _relevant_messages = [message*2**random_length + k for k in range(2**random_length)]
        _relevant_codewords = {k: codebook[k] for k in _relevant_messages}
        h_zm.append(estimate_differential_entropy(_relevant_codewords,
                                                  noise_var, samples, **channel_options))
    h_zm = np.mean(h_zm)
    print(h_z, h_zm)
    return np.maximum((h_z - h_zm)/np.log(2), 0)

def calc_leakage_all_codebooks(codebooks, snr, samples, noise_var=None):
    num_cores = cpu_count()
    if noise_var is None:
        noise_var = 1./(2*10**(snr/10.))
    for dirpath, dirnames, filenames in os.walk(codebooks):
        with open(os.path.join(dirpath, "leakage-estimate.dat"), 'w') as res_file:
            res_file.write("wB\twE\tLeak\n")
        codebooks = [os.path.join(dirpath, k) for k in filenames if k.startswith("codewords")]
        Parallel(n_jobs=num_cores)(delayed(_calc_leakage_parallel)(
            codebook, noise_var, samples, dirpath) for codebook in codebooks)

def _calc_leakage_parallel(codebook, noise_var, samples, dirpath):
    _codebook = parsers.read_codebook_file(codebook, wiretap=True)
    _leak = estimate_leakage_from_codebook(_codebook, noise_var, samples)
    print(_leak)
    _weights = os.path.basename(codebook).split("[", 1)[1]
    _weights = _weights.split("]", 1)[0].split(',')
    with open(os.path.join(dirpath, "leakage-estimate.dat"), 'a') as res_file:
        res_file.write("{}\t{}\t{}\n".format(float(_weights[0]),
                                            float(_weights[1]), _leak))

def calc_leakage_polar(codebooks, snr, samples):
    noise_var = 1./(2*10**(snr/10.))
    codebook_file = os.path.join(codebooks, "codebook-all.csv")
    codebook = parsers.read_codebook_file(codebook_file, wiretap=True)
    leak = estimate_leakage_from_codebook(codebook, noise_var, samples)
    print(leak)
    with open(os.path.join(codebooks, "ber-results.dat")) as old_file:
        x = old_file.readlines()
        ber, bler = x[1].strip().split("\t")
    with open(os.path.join(codebooks, "final-results.dat"), 'w') as res_file:
        res_file.write("BER\tBLER\tLeak\n")
        res_file.write("{}\t{}\t{}\n".format(ber, bler, leak))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("codebooks", help='Directory containing the codebooks')
    parser.add_argument("-s", "--snr", help="SNR as Es/N0", type=float, default=8.341202633917002)#-0.59021942641668)
    #parser.add_argument("--samples", help='Number of samples for MC estimation', type=int, default=2000)
    parser.add_argument("--samples", help='Number of samples for MC estimation', type=int, default=10000)
    parser.add_argument("--polar", action='store_true')
    args = vars(parser.parse_args())
    polar = args.pop("polar")
    if polar:
        calc_leakage_polar(**args)
    else:
        calc_leakage_all_codebooks(**args)

if __name__ == "__main__":
    main()

import argparse
import os

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

RESULTS_FILE = "final-results.dat"

def main(results_dir):
    for root, dirs, files in os.walk(results_dir):
        if RESULTS_FILE not in files:
            continue
        data = pd.read_csv(os.path.join(root, RESULTS_FILE), '\t')
        print(data)
        plt.semilogx(data['BLER'], data['Leak'], 'o-', label=root)
    plt.legend()
    plt.xlabel("BLER")
    plt.ylabel("Leakage [bit]")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("results_dir")
    args = vars(parser.parse_args())
    main(**args)
    plt.show()

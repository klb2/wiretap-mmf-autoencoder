# Wiretap Code Design for Optical Multi-Mode Fiber Channels

This Python3 framework allows designing wiretap codes with finite blocklength
for optical multi-mode fiber channels.
It uses Keras (with Tensorflow backend) for the neural network implementation.

This repository is part of the ICASSP 2020 submission ["Neural Network Wiretap
Code Design for Multi-Mode Fiber Optical
Channels"](https://ieeexplore.ieee.org/document/9053933)
([doi:10.1109/ICASSP40776.2020.9053933](https://doi.org/10.1109/ICASSP40776.2020.9053933)).
It is based on the code from the IEEE TIFS paper
["Wiretap Code Design by Neural Network Autoencoders"](https://doi.org/10.1109/TIFS.2019.2945619),
which can be found here:
[https://gitlab.com/klb2/autoencoder-wiretap](https://gitlab.com/klb2/autoencoder-wiretap).


## Usage
You can run the `autoencoder_wiretap.py` script to start a simulation.
If you want to modify the parameters, e.g. code parameters or the autoencoder
structure, modify the variables in the `main()` function call.

A comparison with a polar wiretap code can be found in the script
`polar_wiretap_comparsion.py`.

The Python script in the `analysis` folder can be used to estimate the entropy 
of found codebooks, using Monte Carlo simulations. The code is based on
[https://github.com/btracey/mixent](https://github.com/btracey/mixent).

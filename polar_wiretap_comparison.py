# Autoencoders for flexible wiretap code design in Python
#
# Copyright (C) 2019 Karl-Ludwig Besser
# License: GPL Version 3

import os.path

import numpy as np
from digcommpy import messages, decoders, encoders, channels, modulators, metrics
from digcommpy import information_theory as it

def main(n=16, info_length=4, random_length=3, test_snr=5., alg='ref'):
    channel = "BAWGN"
    encoder = encoders.PolarWiretapEncoder(n, channel, channel,
                                           info_length_bob=info_length+random_length,
                                           random_length=random_length)
    k = encoder.info_length
    k_bob = encoder.info_length_bob
    print("k={}\tk_bob={}".format(k, k_bob))
    modulator = modulators.BpskModulator()
    channel = channels.BawgnChannel(test_snr, rate=k_bob/n, input_power=1.)
    if alg == "ref":
        decoder = decoders.PolarWiretapDecoder(n, 'BAWGN', test_snr,
                                               pos_lookup=encoder.pos_lookup)
    else:
        raise NotImplementedError("Only the standard polar decoder is "
                                  "implemented right now.")
    info_book, code_book, random_book = encoder.generate_codebook(return_random=True)
    code_book_mod = modulator.modulate_symbols(code_book)
    write_codebook_files(info_book, code_book_mod, random_book)
    #test_set = messages.generate_data(k, number=100000, binary=True)
    test_set = messages.generate_data(k, number=1000000, binary=True)
    test_code = encoder.encode_messages(test_set)
    test_mod = modulator.modulate_symbols(test_code)
    rec_mod = channel.transmit_data(test_mod)
    pred_info = decoder.decode_messages(rec_mod, channel)
    ber = metrics.ber(test_set, pred_info)
    bler = metrics.bler(test_set, pred_info)
    print("BER:\t{}\nBLER:\t{}".format(ber, bler))
    results_file = "Polar_{0}-n{1}-k{2}-r{3}-B{4}E{5}.dat".format(
        alg, n, k, k_bob-k, test_snr, test_snr)
    results_file = os.path.join("results", results_file)
    with open(results_file, 'w') as outf:
        outf.write("BER\tBLER\n")
        outf.write("{}\t{}\n".format(ber, bler))
    return ber, k

def write_codebook_files(messages, codewords, random_mess):
    with open(os.path.join("results", "codebook-all.csv"), 'w') as outfile:
        outfile.write("message\trandom\tcodeword\n")
        for _message, _codeword, _random in zip(messages, codewords, random_mess):
            #outfile.write("\"{}\",\"{}\"\n".format(list(_message), list(_codeword)))
            outfile.write("{}\t{}\t{}\n".format(list(_message), list(_random), list(_codeword)))
    idx_rev = np.unique(messages, axis=0, return_inverse=True)[1]
    for num, _mess_idx in enumerate(np.unique(idx_rev)):
        _idx = np.where(idx_rev == _mess_idx)[0]
        _relevant_codewords = codewords[_idx]
        _relevant_message = messages[_idx]
        with open(os.path.join("results", "codebook-{}.csv".format(num)), 'w') as outfile:
            for _message, _codeword in zip(_relevant_message, _relevant_codewords):
                outfile.write("\"{}\",\"{}\"\n".format(list(_message), list(_codeword)))

if __name__ == "__main__":
    #test_snr = -0.59021942641668
    test_snr = 8.341202633917002
    n = 8
    k = 2
    r = 4
    results = main(n=n, info_length=k, random_length=r, test_snr=test_snr, alg='ref')

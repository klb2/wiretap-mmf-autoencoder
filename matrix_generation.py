import numpy as np
from keras import backend as K
import tensorflow as tf

def generate_np(n=16, mdl=10):
    mdl = 10**(mdl/10.)
    vmax = 1
    vmin = vmax/mdl
    v = (vmax-vmin)*np.random.rand(n) + vmin
    v = np.sort(v)
    #v[0] = vmin
    #v[-1] = vmax
    v = n*v/sum(v)
    assert np.allclose(sum(v), n)
    D = np.diag(v)
    print(v)

def generate_tf(n=16, mdl=10):
    x = np.random.randint(-2, 3, (2, n))
    x = K.variable(x)
    mdl = 10**(mdl/10.)
    vmax = 1
    vmin = vmax/mdl
    v = K.random_uniform((n,), minval=vmin, maxval=vmax)
    v = tf.nn.top_k(v, n)[0]
    v = n*v/K.sum(v)
    print(K.eval(v))
    y = v*x
    print(K.eval(y))


if __name__ == "__main__":
    generate_np()
    generate_tf()
